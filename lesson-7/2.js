/**
 * Доработайте функцию что бы она возвращала объект из переданного вложенного массива
 * 
 * Фукнция принимает 1 аргумента
 * 1. Массив из массивов который содержит 2 элемента — [ [ element1, element2 ] ]
 * 
 * ЗАПРЕЩЕНО ИСПОЛЬЗОВАТЬ ВСТРОЕННЫЙ МЕТОД Object.fromEntries
 * 
 * Обратите внимание!
 * 1. Генерировать ошибку если второй элемент вложенного массива не число, не строка или не null
 * 2. Обязательно использовать деструктуризацию при извлечении элементов массива
 * 3. Если в качестве второго аргумента был передан массив вида [ [ element1, element2 ] ], то его так же нужно преобразовать в объект
 * 4. Для перебора массива можно воспользоваться циклом for..of.
*/

const fromEntries = (entries) => {
    const obj = {};

    return obj;
};

console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
console.log(fromEntries([['name', 'John'], ['address', [['city', 'New  York']]]])); // { name: 'John', address: { city: 'New  York' } }

/// Решение

class ValidationError extends Error {
    constructor(message) {
      super(message);
      this.name = "ValidationError";
    }
}

// отдельная функция для валидации типа второго элемента. 

const check = (element) => {
    const allowed = ['string', 'number', null];
    const isAllowed = allowed.some((item) => typeof element===item);
    if (!isAllowed) {
        throw new ValidationError ('Ошибка: значения ключей могут быть только строкой, числом или null');
    }    
}

 
const fromEntries = (entries) => {
    const result = {};
    for (let [ key, value ] of entries) {
        if (Array.isArray(value)) {
            value = fromEntries(value);
            result[key] = value;
        }
        else {
        check(value);
        result[key] = value;
        }
    }   
    return result;
}; 
 
  
 console.log(fromEntries([['name', 'John'], ['age', 35]])); // { name: 'John', age: 35 }
 console.log(fromEntries([['name', 'John'], ['address', [['city', 'Voronezh']]]])); // { name: 'John', address: { city: 'New  York' } }
      