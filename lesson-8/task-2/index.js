/**
 * Доработать форму из 1-го задания.
 * 
 * Добавить обработчик сабмита формы.
 * 
 * Для того что бы увидеть результат откройте index.html файл в браузере.
 * 
 * Обязательно!
 * 1. При сабмите формы страница не должна перезагружаться
 * 2. Генерировать ошибку если пользователь пытается сабмитить форму с пустыми или содержащими только пробел(ы) полями.
 * 3. Если поля формы заполнены и пользователь нажимает кнопку Вход → вывести в консоль объект следующего вида
 * {
 *  email: 'эмейл который ввёл пользователь',
 *  password: 'пароль который ввёл пользователь',
 *  remember: 'true/false'
 * }
*/

// РЕШЕНИЕ

// Сама форма
const div = document.createElement('div');
div.className = "form-group";
form.prepend(div);

const label = document.createElement('label');
label.setAttribute("for", "email");
label.textContent = "Электропочта";
div.appendChild(label);

const input = document.createElement('input');
input.setAttribute("type", "email");
input.setAttribute("class", "form-control");
input.setAttribute("id", "email");
input.setAttribute("placeholder", "Введите свою электропочту");
div.appendChild(input);

//
const div2 = document.createElement('div');
div2.className = "form-group";
form.append(div2);

const label2 = document.createElement('label');
label2.setAttribute("for", "password");
label2.textContent = "Пароль";
div2.appendChild(label2);

const input2 = document.createElement('input');
input2.setAttribute("type", "password");
input2.setAttribute("class", "form-control");
input2.setAttribute("id", "password");
input2.setAttribute("placeholder", "Введите пароль");
div2.appendChild(input2);

//
const div3 = document.createElement('div');
div3.className = "form-group form-check";
form.append(div3);

const input3 = document.createElement('input');
input3.setAttribute("type", "checkbox");
input3.setAttribute("class", "form-check-input");
input3.setAttribute("id", "exampleCheck1");
div3.appendChild(input3);

const label3 = document.createElement('label');
label3.setAttribute("class", "form-check-label");
label3.setAttribute("for", "exampleCheck1");
label3.textContent = "Запомнить меня";
div3.appendChild(label3);

//
const btn = document.createElement('button');
btn.setAttribute("type", "submit");
btn.setAttribute("class", "btn-primary");
btn.textContent = "Вход";
form.append(btn);


//Обработка формы

document.getElementById('form').addEventListener('submit', () => {
    event.preventDefault();
    const user = {};
    const email = document.getElementById('email').value;
    if (!email) {
        return alert ('Укажите электронную почту');
    } 
    if (email.trim().indexOf(' ') !== -1) {
        return alert ('Укажите корректную электронную почту');
    } else {
        user.email = email;
    } 

    const password = document.getElementById('password').value;
    if (!password) {
        return alert ('Укажите пароль');
    } 
    if (password.indexOf(' ') !== -1) {
        return alert ('Укажите корректный пароль');
    } else {
        user.password = password;
    } 

    if (document.getElementById('exampleCheck1').checked){
        user.remember = true;
    } else {
        user.remember = false;
    }

    return console.log(user);
})