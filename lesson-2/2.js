// Перепишите `if..else` с использованием нескольких операторов `?`. 
// Для читаемости — оформляйте код в несколько строк.

////////////////// Задание //////////////////
// let message;

// if (login === 'Pitter') {
//   message = 'Hi';
// } else if (login === 'Owner') {
//   message = 'Hello';
// } else if (login === '') {
//   message = 'unknown';
// } else {
//   message = '';
// }

////////////////// Решение //////////////////

let message;
let login = '';

message = login === 'Pitter' ? 'Hi' :  login === 'Owner' ? 'Hello' : login === '' ?  'unknown' : '';


//Методом тыка пришла к такому варианту, но он работает только до второго условия. Можно его довести до ума или так вообще не работает?
//let message;
//let login = '';
//
//message = login === 'Pitter' ? 'Hi' : 'Owner' ? 'Hello' : login === '' ? 'unknown' : '';








