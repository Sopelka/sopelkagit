/**
 * Задача 2.
 *
 * Напишите функцию `collect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — число.
 * Массив, который передаётся в аргументе может быть одноуровневым или многоуровневым.
 * Число, которое возвращает функция должно быть суммой всех элементов
 * на всех уровнях всех вложенных массивов.
 *
 * Если при проходе всех уровней не было найдено ни одного числа,
 * то функция должна возвращать число 0.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива reduce.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - Если на каком-то уровне было найдено не число и не массив.
 */

// Решение

function collect (array) {
	if (!Array.isArray(array)){
		return 'Ошибка: аргумент - не массив';
	}
	let result = array.flat(Infinity); 
	if (result.filter(element => typeof element !== 'number')){
		return 'Что то не так с массивом!'
	}else{
	return result.reduce(accumulator, currentValue => accumulator = accumulator + currentValue)
	}
} 



const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
console.log(collect(array1)); // 12

const array2 = [[[[[1, 2]]]]];
console.log(collect(array2)); // 3

const array3 = [[[[[1, 2]]], 2], 1];
console.log(collect(array3)); // 6

const array4 = [[[[[]]]]];
console.log(collect(array4)); // 0

const array5 = [[[[[], 3]]]];
console.log(collect(array5)); // 3

const array = 5;
//console.log(collect(array1)); // 12

const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
//console.log(collect(array1)); // 12

const array2 = [[[[[1, 2]]]]];
//console.log(collect(array2)); // 3

const array3 = [[[[[1, 2]]], 2], 1];
//console.log(collect(array3)); // 6

const array4 = [[[[[]]]]];
//console.log(collect(array4)); // 0

const array5 = [[[[[], 3, 'dshs']]]];
//console.log(collect(array5)); // 3

///Вариант 1

function collect (array) {
	if (!Array.isArray(array)){
		return 'Ошибка: Аргумент - не массив';
	}
	let result = array.flat(Infinity); 
	if (result.some(element => typeof element !== 'number')){
  		return 'Что-то не так с массивом!';
	}else{
  		return result.reduce((acc, element) => acc = acc+element, 0);
	}
}

console.log(collect(array5));

///Вариант 2

function myCollect (arr){
    if (Array.isArray(arr)) {
	    return arr.flat(Infinity).reduce(function(accum, current){
      	return typeof current === 'number' ? accum + current : 'Ошибка: Массив состоит не только из чисел.' 
    },0);
    }else{
    	return 'Ошибка: Аргумент не является массивом.';
    }}
    
console.log(myCollect(array3)); 
