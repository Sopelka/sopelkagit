/**
 * Задача 5.
 *
 * Вручную создать имплементацию функции `reduce`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенные методы Array.prototype.reduce и Array.prototype.reduceRight использовать запрещено;
 * - Третий аргумент функции reduce является не обязательным;
 * - Если третий аргумент передан — он станет начальным значением аккумулятора;
 * - Если третий аргумент не передан — начальным значением аккумулятора станет первый элемент обрабатываемого массива.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция;
 */


//////Решение 
const array = [1, 2, 3, 4, 5];
let INITIAL_ACCUMULATOR= -6 ; 

function condition (accumulator, item){
    return accumulator+item;
}

function myReduce (array, func){
    if (Array.isArray(array) && typeof func == 'function') {
	    let	accumulator = INITIAL_ACCUMULATOR === undefined ? array[0] : INITIAL_ACCUMULATOR;
        array.forEach(function(item){
			accumulator= func(accumulator,item);
		})
        return accumulator;
    }else{
    	return 'Ошибка';
    }
}
console.log(myReduce(array, condition));
