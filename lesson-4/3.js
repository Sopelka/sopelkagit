/**
 * Задача 1.
 *
 * Напишите функцию `inspect`, которая будет принимать массив в качестве аргумента.
 * Возвращаемое значение функции — новый массив.
 * Этот новый массив должен содержать исключительно длины строк, которые были в
 * переданном массиве.
 * Если строк в переданном массиве не было — нужно вернуть пустой массив.
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива filter;
 * - Обязательно использовать встроенный метод массива map.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив.
 */

const array = [
    false,
    'Привет.',
    2,
    'Здравствуй.',
    [],
    'Прощай.',
    {
        name: 'Уолтер',
        surname: 'Уайт',
    },
    'Приветствую.',
];

///Вариант 1
function inspect (array) {
	if (!Array.isArray(array)){
		return 'Ошибка: аргумент - не массив';
	}
	return array.filter(element => typeof element == 'string').map(element => element.length);
} 

console.log(inspect(array));

///Вариант 2
function myInspect (arr){
    if (Array.isArray(arr)) {
	    let StrArr = arr.filter(function(element){
      	return typeof element === 'string';
      });
        let LengthArr= StrArr.map(function(element){
        return element.length;
        });
        return LengthArr;
    }else{
    	return 'Ошибка: Аргумент не является массивом.';
    }}

console.log(myInspect(array));









































































