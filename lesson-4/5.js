/**
 * Задача 3.
 *
 * Напишите функцию `createArray`, которая будет создавать массив с заданными значениями.
 * Первым параметром функция принимает значение, которым заполнять массив.
 * А вторым — количество элементов, которое должно быть в массиве.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента были переданы не число, не строка, не объект и не массив;
 * - В качестве второго аргумента был передан не число.
 */

// Решение

const result = createArray('x', 5);

console.log(result); // [ x, x, x, x, x ]

exports.createArray = createArray;

//////

function create(element, times){
    if (typeof element !== 'string' && 'number' && 'object'){
        return 'Ошибка: первым аргументом может быть только строка, число, массив или объект.';
    }
    if (typeof times !=='number'){
        return 'Ошибка: вторым аргументом может быть только число.';
    }
    let newArray =[];
    for (times; times!=0; times--){
        newArray.push(element);
    }
    return newArray;
}


console.log(create(3, 'rtg'));


