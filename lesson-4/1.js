/**
 * Задача 2.
 *
 * Вручную создать имплементацию функции `filter`.
 * Логика работы ручной имплементации должна быть такой-же,
 * как и у встроенного метода.
 *
 * Заметки:
 * - Встроенный метод Array.prototype.filter использовать запрещено.
 *
 * Генерировать ошибки, если:
 * - В качестве первого аргумента был передан не массив;
 * - В качестве второго аргумента была передана не функция.
 *
 * Заметки:
 * - Второй аргумент встроенного метода filter (thisArg) имплементировать не нужно.
 */



/////Решение

const array = ['Доброе утро!', 'Добрый вечер!', 3, 512, '#', 'До свидания!','До свидания!'];

function condition(item){
	return item === 3;  
}

function philter(arr, func) { 
	const filteredArr = [];
	if (Array.isArray(arr) && typeof func == 'function') {
		arr.forEach(function(item){
			if (func(item)) {
				filteredArr.push(item);
			}
		})
	}else{
		"Первый аргумент - не массив или второй аргумент - не функция"
	}
	return filteredArr;
};


console.log(philter(array, condition));
