/**
 * Задача 3.
 *
 * Создайте функцию createFibonacciGenerator().
 *
 * Вызов функции createFibonacciGenerator() должен возвращать объект, который содержит два метода:
 * - print — возвращает число из последовательности Фибоначчи;
 * - reset — обнуляет последовательность и ничего не возвращает.
 *
 * Условия:
 * - Задачу нужно решить с помощью замыкания.
 */

// РЕШЕНИЕ

const generator1 = createFibonacciGenerator();

console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2
console.log(generator1.print()); // 3
console.log(generator1.reset()); // undefined
console.log(generator1.print()); // 1
console.log(generator1.print()); // 1
console.log(generator1.print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2.print()); // 1
console.log(generator2.print()); // 1
console.log(generator2.print()); // 2

exports.createFibonacciGenerator = createFibonacciGenerator;

///////////////////////Решение

function createFibonacciGenerator() {
	let a = 1;    
	let b = 1;
	let c = 0;
    return {
		print: function() {
			while (c < 2) {
				c++;
				return 1;
			}
			c = a + b;
			a = b;
			b = c;
			return c;
		},
		reset: function() {
			a = 1;
			b = 1;
			c = 0;
		},
	};
}


const generator2 = createFibonacciGenerator();

const generator1 = createFibonacciGenerator();


console.log(generator2.print());
console.log(generator2.print());
console.log(generator1.print());
console.log(generator1.print());
console.log(generator2.print());
console.log(generator2.print());
console.log(generator1.print());
console.log(generator1.print());