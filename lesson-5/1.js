/**
 * Задача 1.
 *
 * Создайте функцию `f`, которая возвращает куб числа, переданного в качестве аргумента.
 *
 * Условия:
 * - Генерировать ошибку, если в качестве аргумента был передан не числовой тип.
 */

// РЕШЕНИЕ


function coub (number){
    if (typeof number !== 'number'){
        return 'Ошибка: Аргумент должен быть числом';
    }
    return number**3;
}


console.log(coub(3));

