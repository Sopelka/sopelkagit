/**
 * Задача 2.
 *
 * Создайте функцию `f`, которая возвращает сумму всех переданных числовых аргументов.
 *
 * Условия:
 * - Функция должна принимать любое количество аргументов;
 * - Генерировать ошибку, если в качестве любого входного аргумента было предано не число.
 */

// РЕШЕНИЕ

///////////// Вариант 1
function isNumber (element){
	return typeof element === 'number';
}

function f(...elements){
	if (elements.length === 0){
		return 'Ошибка: Аргумент не передан';
	}
	if (elements.every(isNumber) === false){
		return 'Ошибка: Аргумент должен быть числом';
	}
	let result = elements.reduce(function (accumulator, currentValue){
		return accumulator + currentValue;
	});
	return result;
};
	
console.log(f(1)); // 9

///////////// Вариант 2
function f(...arg){
	if (arg.length === 0){
		return 'Ошибка: Аргумент не передан';
	}
	return arg.reduce(function(acc, current){
		if (typeof current !== 'number') {
			return 'Ошибка: Аргумент должен быть числом';
		}
		return acc+current;
	});
};

console.log(f(3,2,4,5));









