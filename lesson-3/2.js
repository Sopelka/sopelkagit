/**
 * Задача 2.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо присвоить переменной result значение true, 
 * если строка source содержит подстроку spam. Иначе — false.
 *
 * Условия:
 * - Необходимо выполнить проверку что source и spam являются типом string.
 * - Строки должны быть не чувствительными к регистру
 */

function checkSpam(source, spam) {
    let result =  typeof source === "string" && typeof spam === "string" ? source.toUpperCase().includes(spam.toUpperCase()) : "Убейся, это не строка!";
    return result;
}

console.log(checkSpam('abcde','ABc'));