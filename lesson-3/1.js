/**
 * Задача 1.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо преобразовать первый символ переданной строки в заглавный.
 *
 * Условия:
 * - Необходимо проверить что параметр str является строкой
 */


function upperCaseFirst(str) {
    const result = typeof str === "string" ? str[0].toUpperCase() + str.substring(1, str.length): 'Это не строка!'; 
	return result;
}

console.log(upperCaseFirst('кажется, мне не нравится программирование'));
