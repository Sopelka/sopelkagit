/**
 * Задача 3.
 *
 * Дописать требуемый код что бы код работал правильно.
 * Необходимо проверить длину строки в переменной string.
 * Если она превосходит maxLength – заменяет конец string на ... таким образом, чтобы её длина стала равна maxLength.
 * В консоль должна вывестись (при необходимости) усечённая строка.
 *
 * Условия:
 * - Переменная string должна обладать типом string;
 * - Переменная maxLength должна обладать типом number.
 */

function truncate(string, maxLength) {
    if (typeof string !== "string" || typeof maxLength !== "number") {
        return "Переменная string должна быть строкой, а maxLength - числом!1";
    } else { 
        if (string.length >= maxLength) {
      	    return string.slice(0, maxLength);
        } else {
      	    return string + ".".repeat(maxLength -string.length)
      	
        } 
    }
}

console.log(truncate("aaaa", 6)); 